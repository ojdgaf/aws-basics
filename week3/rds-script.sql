CREATE TABLE users (
  id            INT NOT NULL,
  email         VARCHAR(50) NOT NULL,
  PRIMARY KEY   (id),
  UNIQUE        (email)
);

INSERT INTO users VALUES 
  (1, 'john_doe@mail.com'),
  (2, 'jane_doe@mail.com');

SELECT * FROM users;
