# ssh -i "../private.pem" ec2-user@
# aws sqs send-message --region us-west-2 --message-body 'Hello from SQS!' --queue-url 
# aws sqs receive-message --region us-west-2 --queue-url 
# aws sns publish --region us-west-2 --message 'Testing SNS email topic' --topic 

provider "aws" {}

resource "aws_sqs_queue" "sqs_week5" {
  name = "sqs_week5"

  tags = {
    Name = "sqs_week5"
  }
}

resource "aws_sns_topic" "sns_week5" {
  name = "sns_week5"

  tags = {
    Name = "sns_week5"
  }
}

resource "aws_sns_topic_subscription" "sns_email_topic_week5" { # needs to be manually approved!
  topic_arn = aws_sns_topic.sns_week5.arn
  protocol  = "email"
  endpoint  = "replace-with-yours@mail.com"
}

resource "aws_security_group" "sg_week5" {
  name = "sg_week5"

  ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "sg_week5"
  }
}

resource "aws_iam_role" "role_week5" {
  name = "role_week5"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })

  inline_policy {
    name = "sqs_allow_policy"

    policy = jsonencode({
      Version = "2012-10-17"
      Statement = [
        {
          Action   = ["sqs:*"]
          Effect   = "Allow"
          Resource = "*"
        },
      ]
    })
  }

  inline_policy {
    name = "sns_allow_policy"

    policy = jsonencode({
      Version = "2012-10-17"
      Statement = [
        {
          Action   = ["sns:*"]
          Effect   = "Allow"
          Resource = "*"
        },
      ]
    })
  }
}

resource "aws_iam_instance_profile" "profile_week5" {
  name = "profile_week5"
  role = aws_iam_role.role_week5.name
}

resource "aws_instance" "ec2_week5" {
  ami                         = "ami-08e2d37b6a0129927"
  instance_type               = "t2.micro"
  key_name                    = "replace-with-yours"
  associate_public_ip_address = true
  security_groups             = ["sg_week5"]
  iam_instance_profile        = aws_iam_instance_profile.profile_week5.name

  tags = {
    Name = "ec2_week5"
  }
}

output "ec2_public_ip" {
  description = "EC2 public IP"
  value       = aws_instance.ec2_week5.public_ip
}

output "sqs_url" {
  description = "SQS URL"
  value       = aws_sqs_queue.sqs_week5.url
}

output "sns_topic_arn" {
  description = "SNS topic ARK"
  value       = aws_sns_topic.sns_week5.arn
}
