provider "aws" {}

resource "aws_security_group" "ssh_http_access" {
  name = "ssh_http_access"

  ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}

resource "aws_iam_role" "ec2_s3_role" {
  name = "ec2_s3_role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })

  inline_policy {
    name = "s3_allow_policy"

    policy = jsonencode({
      Version = "2012-10-17"
      Statement = [
        {
          Action   = ["s3:*"]
          Effect   = "Allow"
          Resource = "*"
        },
      ]
    })
  }
}

resource "aws_iam_instance_profile" "ec2_s3_profile" {
  name = "ec2_s3_profile"
  role = aws_iam_role.ec2_s3_role.name
}

resource "aws_instance" "week2" {
  ami                         = "ami-08e2d37b6a0129927"
  instance_type               = "t2.micro"
  key_name                    = "replace-with-yours"
  associate_public_ip_address = true
  security_groups             = ["ssh_http_access"]
  iam_instance_profile        = aws_iam_instance_profile.ec2_s3_profile.name
  user_data                   = <<-EOF
    #!/bin/bash
    aws s3 cp s3://yburdeinyi-week2/data.txt /
  EOF
}

output "instance_public_ip" {
  description = "E2C Instance public IP"
  value       = aws_instance.week2.public_ip
}
