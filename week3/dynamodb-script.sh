#!/bin/bash

table_name='Countries'
region='us-west-2'

aws dynamodb list-tables

aws dynamodb put-item --table-name $table_name --region $region --item '{ "Id" : { "N" : "1" }, "email" : { "S" : "Ukraine" } }'
aws dynamodb put-item --table-name $table_name --region $region --item '{ "Id" : { "N" : "2" }, "email" : { "S" : "USA" } }'

aws dynamodb get-item --table-name $table_name --region $region --key '{ "Id" : { "N" : "1" } }'
aws dynamodb get-item --table-name $table_name --region $region --key '{ "Id" : { "N" : "0" } }'
