# JARs for the final week

Set of jars ([Google Drive](https://drive.google.com/drive/folders/1B1PfZ6Il24vSda-xPTN8iM3iAhZ_xuvb?usp=sharing)) represents distributed calculator application. All jars require java 8 installed. 

* **calc-client.jar** - client application, should be executed on local machine. Continuously sends HTTP requests to the provided endpoint and prints responses. Command to run client jar:
```shell
java -cp calc-client.jar CalcClient <ELB’s DNS name> 
```

* **calc.jar** - application which is running on public instances, load balancer routes requests to this application. Performs calculation, writes information in Dynamo DB, sends message to SQS, sends notification via SNS. Produces logs in /logs folder in the same directory were jar is placed. Command to run client jar:
```shell
java -jar calc.jar
```

* **persist.jar** - application which is running on instance in private subnet. Reads messages from the SQS, log them in RDS, sends SNS notifications. Produces logs in /logs folder in the same directory were jar is placed. Requires environment variable RDS_HOST with correct RDS address. Command to run client jar:
```shell
java -jar persist.jar
```
