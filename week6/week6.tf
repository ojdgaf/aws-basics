provider "aws" {}

# ----------------- net -----------------

resource "aws_vpc" "vpc" {
  cidr_block       = var.vpc_cidr_block
  instance_tenancy = "default"

  tags = {
    Name = "vpc"
  }
}

resource "aws_subnet" "public_subnets" {
  count                   = length(var.public_subnets)
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = element(var.public_subnets, count.index).cidr_block
  availability_zone       = element(var.public_subnets, count.index).availability_zone
  map_public_ip_on_launch = true

  tags = {
    Name = "public_subnet_${count.index + 1}"
  }
}

resource "aws_subnet" "private_subnets" {
  count             = length(var.private_subnets)
  vpc_id            = aws_vpc.vpc.id
  cidr_block        = element(var.private_subnets, count.index).cidr_block
  availability_zone = element(var.private_subnets, count.index).availability_zone

  tags = {
    Name = "private_subnet_${count.index + 1}"
  }
}

resource "aws_db_subnet_group" "rds_subnet_group" {
  name       = "rds_subnet_group"
  subnet_ids = aws_subnet.private_subnets[*].id
}

# ----------------- IAM, permissions, accesses -----------------

resource "aws_security_group" "public_sg" {
  name   = "public_sg"
  vpc_id = aws_vpc.vpc.id

  dynamic "ingress" {
    for_each = var.sg_tcp_ports
    content {
      description = ingress.value.description
      from_port   = ingress.value.port
      to_port     = ingress.value.port
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "public_sg"
  }
}

resource "aws_security_group" "private_sg" {
  name   = "private_sg"
  vpc_id = aws_vpc.vpc.id

  dynamic "ingress" {
    for_each = var.sg_tcp_ports
    content {
      description = ingress.value.description
      from_port   = ingress.value.port
      to_port     = ingress.value.port
      protocol    = "tcp"
      cidr_blocks = aws_subnet.public_subnets[*].cidr_block
    }
  }

  ingress {
    description = "HTTPS"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"] // override
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "private_sg"
  }
}

resource "aws_security_group" "private_rds_sg" {
  name   = "private_rds_sg"
  vpc_id = aws_vpc.vpc.id

  ingress {
    description = "RDS"
    from_port   = var.rds_port
    to_port     = var.rds_port
    protocol    = "tcp"
    cidr_blocks = aws_subnet.private_subnets[*].cidr_block
  }

  tags = {
    Name = "private_rds_sg"
  }
}

resource "aws_iam_role" "ec2_role" {
  name = "ec2_role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })

  inline_policy {
    name = "s3_allow_policy"

    policy = jsonencode({
      Version = "2012-10-17"
      Statement = [
        {
          Action   = ["s3:*"]
          Effect   = "Allow"
          Resource = "*"
        },
      ]
    })
  }

  inline_policy {
    name = "dynamodb_allow_policy"

    policy = jsonencode({
      Version = "2012-10-17"
      Statement = [
        {
          Action   = ["dynamodb:*"]
          Effect   = "Allow"
          Resource = "*"
        },
      ]
    })
  }

  inline_policy {
    name = "sqs_allow_policy"

    policy = jsonencode({
      Version = "2012-10-17"
      Statement = [
        {
          Action   = ["sqs:*"]
          Effect   = "Allow"
          Resource = "*"
        },
      ]
    })
  }

  inline_policy {
    name = "sns_allow_policy"

    policy = jsonencode({
      Version = "2012-10-17"
      Statement = [
        {
          Action   = ["sns:*"]
          Effect   = "Allow"
          Resource = "*"
        },
      ]
    })
  }
}

resource "aws_iam_instance_profile" "ec2_profile" {
  name = "ec2_profile"
  role = aws_iam_role.ec2_role.name
}

# ----------------- net accessibility -----------------

resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.vpc.id

  tags = {
    Name = "igw"
  }
}

resource "aws_route_table" "public_rt" {
  vpc_id = aws_vpc.vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }

  tags = {
    Name = "public_rt"
  }
}

resource "aws_route_table" "private_rt" {
  vpc_id = aws_vpc.vpc.id

  tags = {
    Name = "private_rt"
  }
}

resource "aws_route_table_association" "public_rta" {
  count          = length(aws_subnet.public_subnets)
  subnet_id      = aws_subnet.public_subnets[count.index].id
  route_table_id = aws_route_table.public_rt.id
}

resource "aws_route_table_association" "private_rta" {
  count          = length(aws_subnet.private_subnets)
  subnet_id      = aws_subnet.private_subnets[count.index].id
  route_table_id = aws_route_table.private_rt.id
}

resource "aws_network_interface" "ni" {
  subnet_id         = aws_subnet.public_subnets[0].id
  source_dest_check = false
  security_groups   = [aws_security_group.public_sg.id]

  tags = {
    Name = "ni"
  }
}

resource "aws_route" "nat_route" {
  destination_cidr_block = "0.0.0.0/0"
  network_interface_id   = aws_network_interface.ni.id
  route_table_id         = aws_route_table.private_rt.id
}

# ----------------- instances -----------------

resource "aws_launch_template" "public_ec2" {
  image_id               = "ami-08e2d37b6a0129927"
  instance_type          = "t2.micro"
  key_name               = "replace-with-yours"
  vpc_security_group_ids = [aws_security_group.public_sg.id]
  depends_on             = [aws_s3_object.calc_s3_object]
  user_data = base64encode(templatefile("${path.module}/public_ec2_template.sh", {
    s3_jar_url : "s3://${aws_s3_object.calc_s3_object.bucket}/${aws_s3_object.calc_s3_object.key}"
  }))

  iam_instance_profile {
    arn = aws_iam_instance_profile.ec2_profile.arn
  }
}

resource "aws_instance" "private_ec2" {
  ami                    = "ami-08e2d37b6a0129927"
  instance_type          = "t2.micro"
  key_name               = "replace-with-yours"
  vpc_security_group_ids = [aws_security_group.private_sg.id]
  subnet_id              = aws_subnet.private_subnets[0].id
  iam_instance_profile   = aws_iam_instance_profile.ec2_profile.name
  depends_on             = [aws_s3_object.persist_s3_object, aws_db_instance.rds]
  user_data = base64encode(templatefile("${path.module}/private_ec2_template.sh", {
    s3_jar_url : "s3://${aws_s3_object.persist_s3_object.bucket}/${aws_s3_object.persist_s3_object.key}"
    rds_address : aws_db_instance.rds.address
  }))

  tags = {
    Name = "private_ec2"
  }
}

resource "aws_instance" "nat_ec2" { # https://kenhalbert.com/posts/creating-an-ec2-nat-instance-in-aws
  ami           = "ami-0d57b2083d719c1e5"
  instance_type = "t2.micro"

  network_interface {
    network_interface_id = aws_network_interface.ni.id
    device_index         = 0
  }

  tags = {
    Name = "nat_ec2"
  }
}

# ----------------- storage, queues -----------------

resource "aws_s3_bucket" "s3" {
  bucket = var.bucket_name

  tags = {
    Name = var.bucket_name
  }
}

resource "aws_s3_object" "calc_s3_object" {
  bucket = aws_s3_bucket.s3.id
  key    = "calc.jar"
  source = "jars/calc.jar"
}

resource "aws_s3_object" "persist_s3_object" {
  bucket = aws_s3_bucket.s3.id
  key    = "persist.jar"
  source = "jars/persist.jar"
}

resource "aws_db_instance" "rds" {
  allocated_storage      = 20
  storage_type           = "gp2"
  engine_version         = "12"
  instance_class         = "db.t2.micro"
  engine                 = var.rds_engine
  port                   = var.rds_port
  db_name                = var.rds_db
  username               = var.rds_user
  password               = var.rds_password
  vpc_security_group_ids = [aws_security_group.private_rds_sg.id]
  db_subnet_group_name   = aws_db_subnet_group.rds_subnet_group.id
  skip_final_snapshot    = true
  publicly_accessible    = false
}

resource "aws_dynamodb_table" "dynamodb" {
  name         = var.dynamodb_table
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = var.dynamodb_fields[0].name

  dynamic "attribute" {
    for_each = var.dynamodb_fields
    content {
      name = attribute.value.name
      type = attribute.value.type
    }
  }
}

resource "aws_sqs_queue" "sqs" {
  name = var.sqs_name

  tags = {
    Name = var.sqs_name
  }
}

resource "aws_sns_topic" "sns" {
  name = var.sns_name

  tags = {
    Name = var.sns_name
  }
}

resource "aws_sns_topic_subscription" "sns_email_subscriptions" { # need to be manually approved!
  for_each  = toset(var.sns_emails)
  topic_arn = aws_sns_topic.sns.arn
  protocol  = "email"
  endpoint  = each.value
}

# ----------------- auto scaling and load balancing -----------------

resource "aws_autoscaling_group" "asg" {
  name                      = "asg"
  vpc_zone_identifier       = aws_subnet.public_subnets[*].id
  desired_capacity          = 2
  max_size                  = 3
  min_size                  = 2
  health_check_grace_period = 300
  health_check_type         = "EC2"
  target_group_arns         = [aws_lb_target_group.tg.arn]

  launch_template {
    id      = aws_launch_template.public_ec2.id
    version = "$Latest"
  }

  tag {
    key                 = "Name"
    value               = "public_ec2"
    propagate_at_launch = true
  }
}

resource "aws_lb_target_group" "tg" {
  name        = "tg"
  port        = 80
  target_type = "instance"
  protocol    = "HTTP"
  vpc_id      = aws_vpc.vpc.id

  health_check {
    path    = "/health"
    port    = 80
    matcher = "200"
  }
}

resource "aws_lb" "lb" {
  internal           = false
  load_balancer_type = "application"
  subnets            = aws_subnet.public_subnets[*].id
  security_groups    = [aws_security_group.public_sg.id]

  tags = {
    Name = "lb"
  }
}

resource "aws_lb_listener" "lbl" {
  load_balancer_arn = aws_lb.lb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.tg.arn
  }
}
