#!/bin/bash

aws s3 mb s3://yburdeinyi-week3 --region us-west-2
aws s3 cp rds-script.sql s3://yburdeinyi-week3
aws s3 cp dynamodb-script.sh s3://yburdeinyi-week3
