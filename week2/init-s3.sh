#!/bin/bash

echo 'Data example for week 2' > data.txt
aws s3 mb s3://yburdeinyi-week2 --region us-east-2
aws s3api put-bucket-versioning --bucket yburdeinyi-week2 --versioning-configuration Status=Enabled
aws s3 mv data.txt s3://yburdeinyi-week2