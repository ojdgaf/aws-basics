#!/bin/bash
echo "s3_jar_url: ${s3_jar_url}" > /tmp/variables.txt
echo "rds_address: ${rds_address}" >> /tmp/variables.txt

sudo su
yum update -y
yum install -y java-1.8.0-openjdk

export RDS_HOST=${rds_address}
aws s3 cp ${s3_jar_url} code.jar
java -jar code.jar
