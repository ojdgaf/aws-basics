# ----------------- net -----------------

variable "vpc_cidr_block" {
  default = "10.0.0.0/16"
}

variable "public_subnets" {
  type = list(any)
  default = [
    {
      cidr_block        = "10.0.1.0/24"
      availability_zone = "us-west-2a"
    },
    {
      cidr_block        = "10.0.2.0/24"
      availability_zone = "us-west-2b"
    },
  ]
}

variable "private_subnets" {
  type = list(any)
  default = [
    {
      cidr_block        = "10.0.3.0/24"
      availability_zone = "us-west-2c"
    },
    {
      cidr_block        = "10.0.4.0/24"
      availability_zone = "us-west-2d"
    }
  ]
}

# ----------------- IAM, permissions, accesses -----------------

variable "sg_tcp_ports" {
  type = list(any)
  default = [
    {
      port        = 22,
      description = "SSH"
    },
    {
      port        = 80,
      description = "HTTP"
    },
    {
      port        = 443,
      description = "HTTPS"
    }
  ]
}

# ----------------- RDS -----------------

variable "rds_engine" {
  type    = string
  default = "postgres"
}

variable "rds_port" {
  type    = number
  default = 5432
}

variable "rds_user" {
  type    = string
  default = "rootuser"
}

variable "rds_password" {
  type    = string
  default = "rootuser"
}

variable "rds_db" {
  type    = string
  default = "EduLohikaTrainingAwsRds"
}

# ----------------- S3 -----------------

variable "bucket_name" {
  type    = string
  default = "yburdeinyi-jars"
}

# ----------------- DynamoDB -----------------

variable "dynamodb_table" {
  type    = string
  default = "edu-lohika-training-aws-dynamodb"
}

variable "dynamodb_fields" {
  type = list(any)
  default = [
    {
      name = "UserName",
      type = "S"
    }
  ]
}

# ----------------- SQS -----------------

variable "sqs_name" {
  type    = string
  default = "edu-lohika-training-aws-sqs-queue"
}

# ----------------- SNS -----------------

variable "sns_name" {
  type    = string
  default = "edu-lohika-training-aws-sns-topic"
}

variable "sns_emails" {
  type    = list(any)
  default = ["replace-with-yours@mail.com"]
}
