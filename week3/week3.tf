# ssh -i "../private.pem" ec2-user@
# sh dynamodb-script.sh 
# aws dynamodb get-item --table-name 'Countries' --region 'us-west-2' --key '{ "Id" : { "N" : "2" } }'
# sudo amazon-linux-extras install postgresql12
# psql --version
# psql --port=5432 --username=root --dbname=week3 --host=
# \i rds-script.sql
# insert into users (id, email) values (3, 'psql@mail.com');
# select * from users;

provider "aws" {}

variable "postgres_user" {
  type    = string
  default = "root"
}

variable "postgres_password" {
  type    = string
  default = "root1234"
}

variable "postgres_db" {
  type    = string
  default = "week3"
}

resource "aws_security_group" "ssh_http_access" {
  name = "ssh_http_access"

  ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "ssh_http_access"
  }
}

resource "aws_security_group" "rds_access" {
  name = "rds_access"

  ingress {
    description = "RDS"
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "rds_access"
  }
}

resource "aws_iam_role" "ec2_s3_dynamodb_role" {
  name = "ec2_s3_dynamodb_role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })

  inline_policy {
    name = "s3_allow_policy"

    policy = jsonencode({
      Version = "2012-10-17"
      Statement = [
        {
          Action   = ["s3:*"]
          Effect   = "Allow"
          Resource = "*"
        },
      ]
    })
  }

  inline_policy {
    name = "dynamodb_allow_policy"

    policy = jsonencode({
      Version = "2012-10-17"
      Statement = [
        {
          Action   = ["dynamodb:*"]
          Effect   = "Allow"
          Resource = "*"
        },
      ]
    })
  }
}

resource "aws_iam_instance_profile" "ec2_s3_dynamodb_profile" {
  name = "ec2_s3_dynamodb_profile"
  role = aws_iam_role.ec2_s3_dynamodb_role.name
}

resource "aws_instance" "ec2_week3" {
  ami                         = "ami-08e2d37b6a0129927"
  instance_type               = "t2.micro"
  key_name                    = "replace-with-yours"
  associate_public_ip_address = true
  security_groups             = ["ssh_http_access"]
  iam_instance_profile        = aws_iam_instance_profile.ec2_s3_dynamodb_profile.name
  user_data                   = <<-EOF
    #!/bin/bash
    aws s3 cp s3://yburdeinyi-week3/rds-script.sql /
    aws s3 cp s3://yburdeinyi-week3/dynamodb-script.sh /
  EOF
}

resource "aws_dynamodb_table" "dynamodb_week3" {
  name         = "Countries"
  hash_key     = "Id"
  billing_mode = "PAY_PER_REQUEST"

  attribute {
    name = "Id"
    type = "N"
  }
}

resource "aws_db_instance" "rds_week3" {
  allocated_storage      = 20
  storage_type           = "gp2"
  engine                 = "postgres"
  engine_version         = "12"
  instance_class         = "db.t2.micro"
  db_name                = var.postgres_db
  username               = var.postgres_user
  password               = var.postgres_password
  vpc_security_group_ids = [aws_security_group.rds_access.id]
  skip_final_snapshot    = true
  publicly_accessible    = true
}

output "instance_public_ip" {
  description = "EC2 Instance public IP"
  value       = aws_instance.ec2_week3.public_ip
}

output "rds_endpoint" {
  description = "RDS endpoint"
  value       = aws_db_instance.rds_week3.endpoint
}

output "rds_port" {
  description = "RDS port"
  value       = aws_db_instance.rds_week3.port
}
