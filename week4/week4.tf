provider "aws" {}

# net

resource "aws_vpc" "vpc_week4" {
  cidr_block       = "10.0.0.0/16"
  instance_tenancy = "default"

  tags = {
    Name = "vpc_week4"
  }
}

resource "aws_subnet" "public_subnet_week4" {
  vpc_id                  = aws_vpc.vpc_week4.id
  cidr_block              = "10.0.1.0/24"
  availability_zone       = "us-west-2a"
  map_public_ip_on_launch = true

  tags = {
    Name = "public_subnet_week4"
  }
}

resource "aws_subnet" "private_subnet_week4" {
  vpc_id            = aws_vpc.vpc_week4.id
  cidr_block        = "10.0.2.0/24"
  availability_zone = "us-west-2b"

  tags = {
    Name = "private_subnet_week4"
  }
}

# net accessibility

resource "aws_internet_gateway" "igw_week4" {
  vpc_id = aws_vpc.vpc_week4.id

  tags = {
    Name = "igw_week4"
  }
}

resource "aws_route_table" "public_rt_week4" {
  vpc_id = aws_vpc.vpc_week4.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw_week4.id
  }

  tags = {
    Name = "public_rt_week4"
  }
}

resource "aws_route_table" "private_rt_week4" {
  vpc_id = aws_vpc.vpc_week4.id

  tags = {
    Name = "private_rt_week4"
  }
}

resource "aws_route_table_association" "public_rta_week4" {
  subnet_id      = aws_subnet.public_subnet_week4.id
  route_table_id = aws_route_table.public_rt_week4.id
}

resource "aws_route_table_association" "private_rta_week4" {
  subnet_id      = aws_subnet.private_subnet_week4.id
  route_table_id = aws_route_table.private_rt_week4.id
}

resource "aws_network_interface" "ni_week4" {
  subnet_id         = aws_subnet.public_subnet_week4.id
  source_dest_check = false
  security_groups   = [aws_security_group.public_sg_week4.id]

  tags = {
    Name = "ni_week4"
  }
}

resource "aws_route" "nat_route_week4" {
  destination_cidr_block = "0.0.0.0/0"
  network_interface_id   = aws_network_interface.ni_week4.id
  route_table_id         = aws_route_table.private_rt_week4.id
}

# instances

resource "aws_security_group" "public_sg_week4" {
  name   = "public_sg_week4"
  vpc_id = aws_vpc.vpc_week4.id

  ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "HTTPS"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "public_sg_week4"
  }
}

resource "aws_security_group" "private_sg_week4" {
  name   = "private_sg_week4"
  vpc_id = aws_vpc.vpc_week4.id

  ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["10.0.1.0/24"]
  }

  ingress {
    description = "HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["10.0.1.0/24"]
  }

  ingress {
    description = "HTTPS"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "private_sg_week4"
  }
}

resource "aws_instance" "public_ec2_week4" {
  ami                    = "ami-08e2d37b6a0129927"
  instance_type          = "t2.micro"
  key_name               = "replace-with-yours"
  vpc_security_group_ids = [aws_security_group.public_sg_week4.id]
  subnet_id              = aws_subnet.public_subnet_week4.id
  user_data              = <<EOF
    #!/bin/bash
    sudo su
    yum update -y
    yum install httpd -y
    service httpd start
    chkconfig httpd on
    cd /var/www/html
    echo "<html><h1>Public EC2</h1></html>" > index.html
  EOF

  tags = {
    Name = "public_ec2_week4"
  }
}

resource "aws_instance" "private_ec2_week4" {
  ami                    = "ami-08e2d37b6a0129927"
  instance_type          = "t2.micro"
  key_name               = "replace-with-yours"
  vpc_security_group_ids = [aws_security_group.private_sg_week4.id]
  subnet_id              = aws_subnet.private_subnet_week4.id
  user_data              = <<EOF
    #!/bin/bash
    sudo su
    yum update -y
    yum install httpd -y
    service httpd start
    chkconfig httpd on
    cd /var/www/html
    echo "<html><h1>Private EC2</h1></html>" > index.html
  EOF

  tags = {
    Name = "private_ec2_week4"
  }
}

resource "aws_instance" "nat_ec2_week4" { # https://kenhalbert.com/posts/creating-an-ec2-nat-instance-in-aws
  ami           = "ami-0d57b2083d719c1e5"
  instance_type = "t2.micro"

  network_interface {
    network_interface_id = aws_network_interface.ni_week4.id
    device_index         = 0
  }

  tags = {
    Name = "nat_ec2_week4"
  }
}

# load balancing

resource "aws_lb_target_group" "tg_week4" {
  name        = "tg-week4"
  port        = 80
  target_type = "instance"
  protocol    = "HTTP"
  vpc_id      = aws_vpc.vpc_week4.id

  health_check {
    path    = "/index.html"
    port    = 80
    matcher = "200"
  }
}

resource "aws_lb_target_group_attachment" "public_tga_week4" {
  target_group_arn = aws_lb_target_group.tg_week4.arn
  target_id        = aws_instance.public_ec2_week4.id
  port             = 80
}

resource "aws_lb_target_group_attachment" "private_tga_week4" {
  target_group_arn = aws_lb_target_group.tg_week4.arn
  target_id        = aws_instance.private_ec2_week4.id
  port             = 80
}

resource "aws_lb" "lb_week4" {
  internal           = false
  load_balancer_type = "application"
  subnets            = [aws_subnet.public_subnet_week4.id, aws_subnet.private_subnet_week4.id]
  security_groups    = [aws_security_group.public_sg_week4.id]

  tags = {
    Name = "lb_week4"
  }
}

resource "aws_lb_listener" "lbl_week4" {
  load_balancer_arn = aws_lb.lb_week4.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.tg_week4.arn
  }
}

# output

output "lb_dns_name" {
  description = "Load Balancer DNS name"
  value       = aws_lb.lb_week4.dns_name
}
