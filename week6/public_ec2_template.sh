#!/bin/bash
echo "s3_jar_url: ${s3_jar_url}" > /tmp/variables.txt

sudo su
yum update -y
yum install -y java-1.8.0-openjdk

aws s3 cp ${s3_jar_url} code.jar
java -jar code.jar
