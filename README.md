## About

This is a result of learning fundamental AWS components using CloudFormation and Terraform. The repo consists of 6 consecutive parts (weeks) each of which is dedicated to a specific service. As a result, a complex infrastructure as code was created and described below.

## Tools & services used
* CloudFormation & Terraform
* AWS: EC2, S3, RDS, DynamoDB, SNS, SQS, VPC, ELB, ASG

## Infrastructure

![image info](infrastructure.png)
VPC with 2 public and 2 private subnets. EC2 instances install Java 8, copy a jar file from S3 and execute it. ASG with at least 2 EC2 instances (one EC2 instance per each public subnet). EC2 instances in public subnets have ELB and SSH/HTTP/HTTPS access from all IPs. EC2 in private subnets have SSH access only from public subnets. NAT EC2 instance is used to download software from the internet in private subnets.

The application is basically a distributed calculator and consists of 3 jars. First jar sends HTTP requests to ELB and is to be run on the local machine. Second jar is executed in public EC2. It performs calculations, writes information into DynamoDB, sends messages to SQS, sends notifications via SNS. The last jar is running in private EC2. It reads messages from SQS, logs them into RDS and sends SNS emails.

## Setup
You would need to replace variables such as bucket names, key pairs and emails with your own values. Everything must be executed in US West (Oregon) Region with the default names for RDS, SNS and SQS because they're hardcoded in the Java application.