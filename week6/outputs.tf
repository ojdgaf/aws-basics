output "rds_address" {
  description = "RDS address"
  value       = aws_db_instance.rds.address
}

output "sqs_url" {
  description = "SQS URL"
  value       = aws_sqs_queue.sqs.url
}

output "sns_topic_arn" {
  description = "SNS ARN"
  value       = aws_sns_topic.sns.arn
}

output "lb_dns_name" {
  description = "Load Balancer DNS name"
  value       = aws_lb.lb.dns_name
}
